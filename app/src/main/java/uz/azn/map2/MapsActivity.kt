package uz.azn.map2

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        // Add a marker in Sydney and move the camera
            val drujba = LatLng(41.3094405, 69.2425493)
        val makro = LatLng(41.3163905, 69.2285874)
        mMap.addMarker(MarkerOptions().position(drujba).title("Xalqlar do'stligin"))
        mMap.addMarker(MarkerOptions().position(makro).title("Samarqand darvoza"))
            ?.setIcon(bitmapDescriptorIcon(this, R.drawable.ic_audiotrack))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(drujba, 15f))
    }
    private fun bitmapDescriptorIcon(
        context: Context,
        @DrawableRes drawable: Int
    ): BitmapDescriptor {
        val background = ContextCompat.getDrawable(context, drawable)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)
        val vector = ContextCompat.getDrawable(context, drawable)
        vector!!.setBounds(40, 20, 0, 0)
        val bitmap = Bitmap.createBitmap(
            background.intrinsicWidth, background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        background.draw(canvas)
        vector.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }
}